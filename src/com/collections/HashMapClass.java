package com.collections;

import java.util.HashMap;

public class HashMapClass {
	public static void main(String[] args) {
		HashMap<Integer,String> map=new HashMap<Integer,String>();
		System.out.println(map);
		map.put(1,"java");
		map.put(2, "c++");
		map.put(3, "python");
		int size = map.size();
		System.out.println(size);
		boolean empty = map.isEmpty();
		System.out.println(empty);
		boolean containsKey = map.containsKey(7);
		System.out.println(containsKey);
		boolean containsValue = map.containsValue("java");
		System.out.println(containsValue);
	
		
		System.out.println(map);
		map.remove(1);
		System.out.println(map);
	}

}
